
from flask import Flask
from flask import request

app = Flask(__name__)

class service() :
        def add(self, a, b) :
                return str(a + b)

        def sub(self, a, b) :
                return str(a - b)

result = service()

@app.route('/')
def index():
    return 'Hello Bob from user146'

@app.route('/add')
def add():
    return result.add(int(request.args.get('a')), int(request.args.get('b')))

@app.route('/sub')
def sub():
    return result.sub(int(request.args.get('a')), int(request.args.get('b')))

app.run(host='0.0.0.0',port=8146)
