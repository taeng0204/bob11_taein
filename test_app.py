import unittest
from app import service

class TestApp(unittest.TestCase) :
        def test_add(self) :
                ex = service()
                result = ex.add(3,5)
                self.assertEqual(result, '8')

        def test_sub(self) :
                ex = service()
                result = ex.sub(3, 5)
                self.assertEqual(result, '-2')

if __name__ == "__main__" :
        unittest.main()